/**
 * Client side code.
 */

(function() {
    "use strict";
    var app = angular.module("PopQuizApp", []);

    app.controller("PopQuizCtrl", ["$http", PopQuizCtrl]);

    function PopQuizCtrl($http) {
        var self = this; // vm

        self.quiz = {
            
        };

        self.finalanswer = {
            id: 0,
            value: "",
            comments: "",
            message: ""
        };

        self.initForm = function() {
            $http.get("/popquizes")
                .then(function(result) {
                    console.log(result);
                    self.quiz = result.data;
                }).catch(function(e) {
                    console.log(e);
                });
        }
    

        self.initForm();

        self.submitQuiz = function() {
            console.log("submitQuiz !!!");
            self.finalanswer.id = self.quiz.id;
            $http.post("/submit-quiz", self.finalanswer)
                .then(function(result) {
                    console.log(result);
                    if (result.data.isCorrect) {
                        self.finalanswer.message = "It's Correct!"
                    } else {
                        self.finalanswer.message = "Sorry, that is incorrect!";
                    }
                }).catch(function(e){
                    console.log(e);
                });
        };

    }

})();