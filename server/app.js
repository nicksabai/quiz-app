/**
 * Server side code.
 */
"use strict";

console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

//Start of quiz app

var quizes = [{
    id: 0,
    question: "Which country has the largest population in the world?",
    answer1: {name: "India", value: 1},
    answer2: {name: "USA", value: 2},
    answer3: {name: "Germany", value: 3},
    answer4: {name: "China", value: 4},
    answer5: {name: "Russia", value: 5},
    answer: 4
},

{
    id: 1,
    question: "In which year did World War II end?",
    answer1: {name: "1945", value: 1},
    answer2: {name: "1947", value: 2},
    answer3: {name: "1953", value: 3},
    answer4: {name: "1938", value: 4},
    answer5: {name: "1955", value: 5},
    answer: 1
},

{
    id: 2,
    question: "What is the name of the galaxy of which our sun is a member?",
    answer1: {name: "Comet Galaxy", value: 1},
    answer2: {name: "Sunflower Galaxy", value: 2},
    answer3: {name: "Milky Way Galaxy", value: 3},
    answer4: {name: "Andromeda Galaxy", value: 4},
    answer5: {name: "Cigar Galaxy", value: 5},
    answer: 3
},

{
    id: 3,
    question: "Which logo on a flag identifies a ship's crew as pirates?",
    answer1: {name: "Bull", value: 1},
    answer2: {name: "Skull and crossbones", value: 2},
    answer3: {name: "Lion", value: 3},
    answer4: {name: "Hammer and sickle", value: 4},
    answer5: {name: "Duck", value: 5},
    answer: 2
},

{
    id: 4,
    question: "What falling object is said to have inspired Isaac Newton's theories about gravity?",
    answer1: {name: "Plum", value: 1},
    answer2: {name: "Orange", value: 2},
    answer3: {name: "Pineapple", value: 3},
    answer4: {name: "Durian", value: 4},
    answer5: {name: "Apple", value: 5},
    answer: 5
},
];

app.get("/popquizes", function(req, res) {
    var x = Math.random() * (6 - 1) + 1; //Randomizes the questions
    var y = Math.floor(x); // Rounds down decimal to integer
    console.log(y - 1);
    res.json(quizes[y - 1]); //Responds to client-side request
});

app.post("/submit-quiz", function(req, res) {
    console.log("Received quest object " + req.body);
    console.log("Received quest object " + JSON.stringify(req.body));
    var quiz = req.body;
    var checking = quizes[quiz.id];
    console.log(checking.answer);
    console.log(quiz.value);
    
    if (checking.answer == parseInt(quiz.value)) {
        console.log("CORRECT!");
        quiz.isCorrect = true;
    } else {
        console.log("INCORRECT!");
        quiz.isCorrect = false;
    }
    res.status(200).json(quiz);
});
// End of quiz app

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});